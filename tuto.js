// classic definition
// export : to use function in other file
export default function DoSomething(){

}

// arrow definition
// export : to use function in other file
export const DoSomething = () => {

}